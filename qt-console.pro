QT -= gui

CONFIG += c++17 console
CONFIG -= app_bundle

SOURCES += \
        main.cpp

HEADERS += \
    utf8LogHandler.h

DESTDIR = $$PWD
