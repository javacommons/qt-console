#! bash -uvx
set -e
astyle --style=allman --recursive  *.java,*.c,*.cpp,*.cxx,*.h,*.hpp,*.hxx
sleep 3
UNAME=`uname`
if [ "$UNAME" = "Windows_NT" ]; then
  mingwx.cmd mk.mgw qt-console
else
  mk.mgw qt-console
fi
./qt-console.exe

